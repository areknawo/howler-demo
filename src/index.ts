import { Howl, Howler } from "howler";
import SimplePeer from "simple-peer";

const record = (stream: MediaStream, seconds: number): Promise<string> => {
  var recordedChunks = [];
  return new Promise((resolve, reject) => {
    const mediaRecorder = new MediaRecorder(stream, {
      mimeType: "video/webm; codec='vp9 vorbis'",
    });

    mediaRecorder.start(seconds * 1000);
    mediaRecorder.addEventListener("dataavailable", (event) => {
      recordedChunks.push(event.data);
      if (mediaRecorder.state === "recording") {
        mediaRecorder.stop();
      }
    });

    mediaRecorder.addEventListener("stop", () => {
      var blob = new Blob(recordedChunks, {
        type: "video/webm",
      });
      var url = URL.createObjectURL(blob);
      resolve(url);
    });
  });
};
const setupConnection = (
  isInitiator: boolean
): Promise<SimplePeer.Instance> => {
  return new Promise((resolve, reject) => {
    const peer = new SimplePeer({
      initiator: isInitiator,
      trickle: false,
    });
    peer.on("error", (error) => {
      console.error(error);
      reject(error);
    });

    peer.on("signal", (data) => {
      localStorage.setItem("signal", JSON.stringify(data));
    });

    window.addEventListener("storage", (ev) => {
      peer.signal(localStorage.getItem("signal"));
    });

    peer.on("connect", () => {
      resolve(peer);
    });
  });
};
const getFile = (): Promise<string> => {
  return new Promise((resolve) => {
    const filePicker = document.getElementById(
      "file-picker"
    ) as HTMLInputElement;
    filePicker.addEventListener("change", () => {
      const file = filePicker.files[0];
      resolve(loadFile(file));
    });
  });
};
const loadFile = (file: File): Promise<string> => {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.onload = (event) => {
      resolve(event.target.result as string);
    };
    fileReader.onerror = (event) => reject(event);
    fileReader.onabort = (event) => reject(event);
    fileReader.readAsDataURL(file);
  });
};
const loadHowler = (): Promise<[MediaStream, Howl]> => {
  return new Promise((resolve) => {
    getFile().then((file) => {
      const format = /data:audio\/(.+?);/.exec(file)[1];
      const howler = new Howl({
        src: [file],
        format: [format],
        loop: true,
      }).on("load", () => {
        const dest = Howler.ctx.createMediaStreamDestination();
        Howler.masterGain.disconnect();
        Howler.masterGain.connect(dest);
        resolve([dest.stream, howler]);
      });
    });
  });
};
const loadUserMedia = async (): Promise<MediaStream> => {
  const fullStream = await navigator.mediaDevices.getUserMedia({
    audio: true,
    video: true,
  });

  return fullStream;
};
const mixStreams = (
  firstStream: MediaStream,
  secondStream: MediaStream
): MediaStream => {
  const ctx = new AudioContext({
    latencyHint: "interactive",
    sampleRate: 44100,
  });
  const dest = ctx.createMediaStreamDestination();
  const firstVideoTracks = firstStream.getVideoTracks();
  const secondVideoTracks = secondStream.getVideoTracks();
  if (firstStream.getAudioTracks().length > 0) {
    ctx.createMediaStreamSource(firstStream).connect(dest);
  }
  if (secondStream.getAudioTracks().length > 0) {
    ctx.createMediaStreamSource(secondStream).connect(dest);
  }
  if (firstVideoTracks.length > 0) {
    dest.stream.addTrack(firstVideoTracks[0]);
  } else if (secondVideoTracks.length > 0) {
    dest.stream.addTrack(secondVideoTracks[0]);
  }

  return dest.stream;
};

const runHost = async (peer: SimplePeer.Instance): Promise<void> => {
  const [howlerStream, howler] = await loadHowler();
  const userMedia = await loadUserMedia();
  const mix = mixStreams(howlerStream, userMedia);
  const audio = document.createElement("audio");
  const audio2 = document.createElement("audio");
  audio.srcObject = mix;
  audio2.srcObject = howlerStream;
  peer.addStream(mix);
  howler.play();
  audio.play();
  audio2.play();
  window.addEventListener("click", () => {
    audio.pause();
    if(audio.srcObject === howlerStream) {
      audio.srcObject = mix;
    } else {
      audio.srcObject = howlerStream;
    }
    audio.play();
    console.log("isHowler", audio.srcObject === howlerStream)
    /*if (audio.paused) {
      audio.play();
    } else {
      audio.pause();
    }*/
  });
};
const runClient = async (peer: SimplePeer.Instance): Promise<void> => {
  const video = document.createElement("video");
  video.setAttribute("width", "400");
  video.setAttribute("height", "400");
  document.body.append(video);

  peer.on("stream", async (stream: MediaStream) => {
    video.srcObject = stream;
    video.play();
  });
  /*video.addEventListener("play", async () => {
    const recordedVideo = await record(video.srcObject as MediaStream, 10);
    window.open(recordedVideo);
  });*/
  window.addEventListener("click", () => {
    if (video.paused) {
      video.play();
    } else {
      video.pause();
    }
  });
};

const run = async (): Promise<void> => {
  const isInitiator = location.hash.includes("initiator");
  const peer = await setupConnection(isInitiator);
  console.log("connected");

  if (isInitiator) {
    runHost(peer);
  } else {
    runClient(peer);
  }
};

run();
